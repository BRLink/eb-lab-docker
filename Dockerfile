FROM php:5.6-fpm
# Install modules
RUN apt-get update && apt-get install -y \
        libfreetype6-dev \
        libjpeg62-turbo-dev \
        libmcrypt-dev \
        libpng12-dev \
    && docker-php-ext-install iconv mcrypt mysql \
    && docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ \
    && docker-php-ext-install gd
# Install and Configure Custom Metrics CloudWatch
RUN apt-get install -y unzip libwww-perl libdatetime-perl wget \
    && mkdir -p /opt/cloudwatch-custom \
    && wget http://aws-cloudwatch.s3.amazonaws.com/downloads/CloudWatchMonitoringScripts-1.2.1.zip -P /opt/cloudwatch-custom \
    && unzip CloudWatchMonitoringScripts-1.2.1.zip -d /opt/cloudwatch-custom \
    && rm -rf CloudWatchMonitoringScripts-1.2.1.zip \
    && chmod u+x /opt/cloudwatch-custom/aws-scripts-mon/mon-put-instance-data.pl \
    && echo '00-59/2 * * * * root /scripts/tools/aws-scripts-mon/mon-put-instance-data.pl --mem-util --mem-used --mem-avail --auto-scaling --from-cron > /dev/null 2>&1' > /etc/crontab
CMD ["php-fpm"]
